-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `pohon`;
CREATE TABLE `pohon` (
  `pohon_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `riap_0` double(4,2) NOT NULL,
  `riap_1` double(4,2) NOT NULL,
  `riap_2` double(4,2) NOT NULL,
  `riap_3` double(4,2) NOT NULL,
  `riap_4` double(4,2) NOT NULL,
  `riap_5` double(4,2) NOT NULL,
  `riap_6` double(4,2) NOT NULL,
  `riap_7` double(4,2) NOT NULL,
  `riap_8` double(4,2) NOT NULL,
  `riap_9` double(4,2) NOT NULL,
  `riap_10` double(4,2) NOT NULL,
  PRIMARY KEY (`pohon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pohon` (`pohon_id`, `name`, `deskripsi`, `riap_0`, `riap_1`, `riap_2`, `riap_3`, `riap_4`, `riap_5`, `riap_6`, `riap_7`, `riap_8`, `riap_9`, `riap_10`) VALUES
(1,	'Sengon',	'<i>Fast growing species</i> (cepat tumbuh), kanopi menyerupai kubah, dapat dipakai untuk bahan baku industri panel kayu dan kayu lapis, warna kulitnya abu hingga kehijauan, tumbuh di berbagai jenis tanah, tumbuh di curah hujan lembab hingga basah.',	2.00,	2.00,	3.00,	3.50,	4.00,	5.00,	3.00,	4.00,	4.00,	3.00,	3.00),
(2,	'Jabon',	'<i>Fast growing species</i>, kayu sedikit lunak (kelas III-IV), kurang awet, kayu untuk perdagangan dan pertukangan, cocok untuk kayu hutan rakyat ataupun industri.',	2.00,	1.20,	1.50,	4.00,	8.00,	11.60,	8.60,	6.60,	4.60,	2.60,	2.00),
(3,	'Suren',	'<i>Fast growing species</i>, biasa diasosiasikan dengan tanaman lain di hutan rakyat, kayu untuk pertukangan atau industri, kayu sedikit lunak.',	2.00,	1.00,	1.20,	3.00,	4.00,	4.00,	5.00,	5.40,	4.30,	4.00,	2.00),
(4,	'Kaliandra',	'Tumbuh cepat pada lahan terbuka, tumbuh di berbagai jenis tanah, tahan asam ataupun rendah unsur hara, dapat mencegah erosi terutama pada kawasan curam, kayunya untuk kayu energi dan kayu bakar, pakan ternak, atau industri, kerapatan cukup tinggi, baik untuk konservasi tanah\r\n',	2.00,	1.00,	2.00,	2.00,	3.00,	4.00,	4.00,	4.00,	3.50,	3.00,	2.00),
(5,	'Mahoni',	'Tajuknya dapat membentuk kanopi payung yang sangat rimbun, batang slindris, percabangan banyak, kayunya keras, mengurangi polusi signifikan (filter udara yang baik).\r\n',	2.00,	1.50,	1.70,	2.00,	2.20,	2.50,	2.20,	2.30,	2.30,	2.10,	1.90),
(6,	'Jati',	'Rata-rata ketinggian 9-11 meter, fungsi untuk furnitur berkualitas, dapat diasosiasikan dengan tanaman pangan di hutan rakyat, polikultur dengan tanaman kehutanan, atau monokultur untuk industri, kulit batang coklat kuning keabu-abuan, sebagian besar batang silindris dan lurus.\r\n',	2.00,	1.38,	1.38,	1.40,	1.40,	2.00,	2.10,	2.44,	2.30,	2.00,	1.50)
ON DUPLICATE KEY UPDATE `pohon_id` = VALUES(`pohon_id`), `name` = VALUES(`name`), `deskripsi` = VALUES(`deskripsi`), `riap_0` = VALUES(`riap_0`), `riap_1` = VALUES(`riap_1`), `riap_2` = VALUES(`riap_2`), `riap_3` = VALUES(`riap_3`), `riap_4` = VALUES(`riap_4`), `riap_5` = VALUES(`riap_5`), `riap_6` = VALUES(`riap_6`), `riap_7` = VALUES(`riap_7`), `riap_8` = VALUES(`riap_8`), `riap_9` = VALUES(`riap_9`), `riap_10` = VALUES(`riap_10`);

-- 2018-11-19 20:16:25
