$(document).ready(function(){
  var i=1;  

  $('#add').click(function(){  
       i++;  
       $('#dynamic_field').append(
          `<tr id="row`+i+`" class="dynamic-added dynamic-item-row"> 
            <td style="border-top:0px; padding: 4px 8px"  class="list-item"><input type="text" name="nama[]" placeholder="Item" class="form-control nama_list" /></td>
            <td style="border-top:0px; padding: 4px 8px"  class="list-item"><input type="text" name="durasi[]" placeholder="Lama Pemakaian" class="form-control durasi_list" /></td>
            <td style="border-top:0px; padding: 4px 8px"  class="list-item"><input type="text" name="energi[]" placeholder="Daya" class="form-control energi_list" /></td>
            <td style="border-top:0px; padding: 4px 8px"  class="list-item">
              <div class="form-group">
                <select class="form-control" id="satuan_list">
                  <option>Tahun</option>
                  <option>Bulan</option>
                  <option>Minggu</option>
                  <option>Hari</option>
                </select>
              </div>
            </td>
            <td style="border-top:0px; padding: 4px 8px"  class="list-item"><button type="button" name="remove" id="`+i+`" class="btn btn-danger btn_remove">X</button></td>
          </tr>`
        );  
  });  

  $(document).on('click', '.btn_remove', function(){  
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();  
  });  

  $(document).on('click', 'input.pilih-pohon', function(){
    $('tr.list-desc').hide();

    var pohon_id = $(this).attr("value");
    $('#nama-pohon-'+pohon_id+'').show();
    $('#deskripsi-pohon-'+pohon_id+'') .show();
  })

  $(document).on('change', '.input_item', function(){
    for (var i = $('.dynamic-item-row').length - 1; i >= 0; i--) {
      $('.dynamic-item-row')[i]
    }
  })

  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });

    var height = $('.jumbotron').height();
    var scrollTop = $(window).scrollTop();

    if (scrollTop >= height - 20) {
        $('.navbar').addClass('solid-nav');
    } else {
        $('.navbar').removeClass('solid-nav');
    }
  });
})

function calc_carbon(wattage) {
  var kwattage = wattage/1000
  return kwattage * 0.719/3.67
}