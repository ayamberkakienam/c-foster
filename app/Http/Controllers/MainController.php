<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        // $pohon = \App\Pohon::all();

        // foreach ($pohon as $ipohon) {
        //     $total_riap = $ipohon['riap_0']+$ipohon['riap_1']+$ipohon['riap_2']+$ipohon['riap_3']+$ipohon['riap_4']+$ipohon['riap_5']+$ipohon['riap_6']+$ipohon['riap_7']+$ipohon['riap_8']+$ipohon['riap_9']+$ipohon['riap_10'];
        //     $ipohon['total_riap'] = $total_riap;
        // }
        
        // return view("home", compact('pohon'));
    	return view("home");
    }

    public function postInstruments(Request $request)
    {
    	$rules = [];

    	foreach ($request->input('name') as $key => $value) {
    		$rules['name.{$key}'] = 'required';
    	}

    	$validator = Validator::make($request->all(), $rules);

    	if ($validator->passes()) {
    		foreach ($request->input('name') as $key => $value) {
    			Instrument::create(['name'=>$value]);
    		}

    		return response()->json(['success'=>'done']);
    	}

    	return response()->json(['error'=>$validator->errors()->all()]);
    }
}
