Due to my inability to properly setup a Heroku MySQL addon, all data are now hardcoded into the front-end.
Original back-end oriented MVC is stored in "legacy-backend" branch. Current no-backend style are backed up on "no-backend" branch.

Todo:
1. Input error hint in textbox
2. Page scroll to result section when button is clicked
