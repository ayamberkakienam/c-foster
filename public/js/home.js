$(document).ready(function(){
  var i=0;  

  $('#add').click(function(){  
       i++;  
       $('#dynamic_field').append(
          `<tr id="row`+i+`" class="dynamic-added dynamic-item-row"> 
            <td class="list-item"><input type="text" name="nama[]" placeholder="Item" class="form-control nama_list input-item" id="input-nama-`+i+`" /></td>
            <td class="list-item"><input type="text" name="durasi[]" placeholder="Lama Pemakaian" class="form-control durasi_list input-item" id="input-durasi-`+i+`" /></td>
            <td class="list-item"><input type="text" name="energi[]" placeholder="Daya" class="form-control energi_list input-item" id="input-energi-`+i+`" /></td>
            <td class="list-item">
              <div class="form-group">
                <select class="form-control input-item" id="input-satuan-`+i+`" >
                  <option>Tahun</option>
                  <option>Bulan</option>
                  <option>Minggu</option>
                  <option>Hari</option>
                </select>
              </div>
            </td>
            <td style="border-top:0px; padding: 4px 8px"  class="list-item"><button type="button" name="remove" id="`+i+`" class="btn btn-danger btn_remove">X</button></td>
          </tr>`
        );  
  });  

  $(document).on('click', '.btn_remove', function(){  
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();  
  });  

  $(document).on('click', 'input.pilih-pohon', function(){
    $('tr.list-desc').hide();

    var pohon_id = $(this).attr("value");
    $('#nama-pohon-'+pohon_id+'').show();
    $('#deskripsi-pohon-'+pohon_id+'') .show();
  })

  $(document).on('change', '.input-item', function(event){
    var watt_sum = 0;
    event.preventDefault();
    for (var i = 0; i < $('.dynamic-item-row').length; i++) {
      var satuan = document.getElementById('input-satuan-'+i+'').value
      switch(satuan) {
        case "Tahun":
          satuan = 365;
          break;
        case "Bulan":
          satuan = 31;
          break;
        case "Minggu":
          satuan = 7;
          break;
        case "Hari":
          satuan = 1;
          break;
        default:
          satuan = 0;
      }

      var durasi = document.getElementById('input-durasi-'+i+'').value;
      console.log(durasi)
      if (!isNaN(durasi = Number(durasi))) {
        durasi = Number(durasi);
      } else {
        durasi = 0;
      }
      var energi = document.getElementById('input-energi-'+i+'').value;
      if (!isNaN(energi = Number(energi))) {
        energi = Number(energi);
      } else {
        energi = 0;
      }

      console.log('row '+i+' | '+'durasi '+durasi+' | '+'energi '+energi+' | '+'satuan '+satuan);
      watt_sum += durasi*energi*satuan;
    }
    console.log(watt_sum);
    var carbon_sum = watt_sum/1000*0.719/3.67;

    document.getElementById('total-emisi-text').innerHTML = "| Total emisi karbon: "+carbon_sum.toFixed(2)+" kg Carbon/tahun";
    $('#total-emisi-hidden').val(carbon_sum);
    
    console.log($('#total-emisi-hidden').val());
  })

  $(document).on('click', '#hitung-btn', function(event){
    event.preventDefault();
    var pohon_id = $('input[name=tipe_pohon]:checked').val();
    var pohon_nama = $('input[name=tipe_pohon]:checked').parent().text();
    var total_riap = $('#total-riap-'+pohon_id+'hidden').val();
    var total_carbon = $('#total-emisi-hidden').val();
    var jarak_tanam = $('#jarak_list').val()

    var biomassa = 0.118*Math.pow(pohon_id, 2.53);
    var jumlah_pohon = Math.ceil(total_carbon/biomassa);
    var luas_lahan = jumlah_pohon * jarak_tanam
    var jumlah_uang = jumlah_pohon*101000

    $('#selected-pohon').text(pohon_nama);
    $('#jumlah-pohon').text(jumlah_pohon);
    $('#luas-lahan').text(luas_lahan.toLocaleString('id'));
    $('#cost-result').text(jumlah_uang.toLocaleString('id'));
  })

  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
  
  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });

    var height = $('.jumbotron').height();
    var scrollTop = $(window).scrollTop();

    if (scrollTop >= height - 20) {
        $('.navbar').addClass('solid-nav');
    } else {
        $('.navbar').removeClass('solid-nav');
    }
  });
})