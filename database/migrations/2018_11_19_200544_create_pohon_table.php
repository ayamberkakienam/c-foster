<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePohonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pohon', function (Blueprint $table) {
            $table->increments('pohon_id');
            $table->string('name');
            $table->longtext('deskripsi');
            $table->float('riap_0', 4, 2);
            $table->float('riap_1', 4, 2);
            $table->float('riap_2', 4, 2);
            $table->float('riap_3', 4, 2);
            $table->float('riap_4', 4, 2);
            $table->float('riap_5', 4, 2);
            $table->float('riap_6', 4, 2);
            $table->float('riap_7', 4, 2);
            $table->float('riap_8', 4, 2);
            $table->float('riap_9', 4, 2);
            $table->float('riap_10', 4, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pohon');
    }
}
