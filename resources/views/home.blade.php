<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>C-Foster Carbon Calculator</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datatables.css') }}" rel="stylesheet">
  <link href="{{ asset('css/home.css') }}" rel="stylesheet">
  <script src="{{ asset('js/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/datatables.min.js') }}"></script>
  
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="background-color:#fff"></span>
        <span class="icon-bar" style="background-color:#fff"></span>
        <span class="icon-bar" style="background-color:#fff"></span>                        
      </button>
      <a class="navbar-brand" href="#myPage"><img src="{{ asset('img/logo-50px.png') }}"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#calculator">CALCULATOR</a></li>
        <li><a href="#contact">CONTACT</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center">
  <div class="centered">
    <h1 class="title">Canopy X-tion</h1> 
    <p class="title">Carbon Neutral Applied to Exsitu in Action</p>
  </div>
</div>

<!-- Container (About Section) -->
<div id="calculator" class="container-fluid">
  <div class="text-center">
    <h2>Carbon Calculator</h2>
    <p>Use the calculator below :D</p>
  </div>
  <div class="calculator row">
    <div class="form-group">
      <form name="add_item" id="add_item">  

        <div class="table-responsive">  
          <table class="table table" id="dynamic_field">  
              <tr>
                <th class="list-item header">Nama Item</th>
                <th class="list-item header">Pemakaian/Hari (Jam)</th>
                <th class="list-item header">Energi (Watt)</th>
                <th class="list-item header">Satuan</th>
                <th class="list-item header">Aksi</th>
              </tr>
              <tr class="dynamic-item-row" id="row0">  
                  <td class="list-item"><input type="text" name="nama[]" placeholder="Item" class="form-control nama_list input-item" id="input-nama-0" /></td>
                  <td class="list-item"><input type="text" name="durasi[]" placeholder="Lama Pemakaian" class="form-control durasi_list input-item" id="input-durasi-0" /></td>  
                  <td class="list-item"><input type="text" name="energi[]" placeholder="Daya" class="form-control energi_list input-item" id="input-energi-0" /></td>
                  <td class="list-item">
                    <div class="form-group">
                      <select class="form-control input-item" id="input-satuan-0" >
                        <option>Tahun</option>
                        <option>Bulan</option>
                        <option>Minggu</option>
                        <option>Hari</option>
                      </select>
                    </div>
                  </td>
              </tr>  
          </table>
          <div class="col-sm-12">
            <button type="button" name="add" id="add" class="btn-add btn btn-info" value="Add more">Tambah item</button>
            <span id="total-emisi-text">
              | Total emisi karbon: 
            </span>
          </div>  
        </div>
        <div class="col-sm-12 tree-group">
          <div class="col-sm-3 tree-group">
            <div class="table-responsive tree">
              <table class="table">
                <tr>
                  <th class="list-item header">Pilihan Pohon</th>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=1> Sengon<br></td>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=2> Jabon<br></td>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=3> Suren<br></td>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=4> Kaliandra<br></td>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=5> Mahoni<br></td>
                </tr>
                <tr>
                  <td class="list-item"><input class="pilih-pohon" type="radio" name="tipe_pohon" value=6> Jati<br></td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-sm-9 description">
            <div class="table-responsive tree-desc">
              <table class="table">
                <tr id="nama-pohon-1" class="list-desc" style="display: none;">
                  <th class="list-desc header">Sengon (Paraserianthes falcataria)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-1" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    <i>Fast growing species</i> (cepat tumbuh), kanopi menyerupai kubah, dapat dipakai untuk bahan baku industri panel kayu dan kayu lapis, warna kulitnya abu hingga kehijauan, tumbuh di berbagai jenis tanah, tumbuh di curah hujan lembab hingga basah.
                  </td>
                </tr>
                <tr id="nama-pohon-2" class="list-desc" style="display: none;">
                  <th class="list-desc header">Jabon (Neolamaeckia cadamba)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-2" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    <i>Fast growing species</i>, kayu sedikit lunak (kelas III-IV), kurang awet, kayu untuk perdagangan dan pertukangan, cocok untuk kayu hutan rakyat ataupun industri.
                  </td>
                </tr>
                <tr id="nama-pohon-3" class="list-desc" style="display: none;">
                  <th class="list-desc header">Suren (Toona sinensis)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-3" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    <i>Fast growing species</i>, biasa diasosiasikan dengan tanaman lain di hutan rakyat, kayu untuk pertukangan atau industri, kayu sedikit lunak.
                  </td>
                </tr>
                <tr id="nama-pohon-4" class="list-desc" style="display: none;">
                  <th class="list-desc header">Kaliandra (Calliandra callothyrsus)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-4" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    Tumbuh cepat pada lahan terbuka, tumbuh di berbagai jenis tanah, tahan asam ataupun rendah unsur hara, dapat mencegah erosi terutama pada kawasan curam, kayunya untuk kayu energi dan kayu bakar, pakan ternak, atau industri, kerapatan cukup tinggi, baik untuk konservasi tanah

                  </td>
                </tr>
                <tr id="nama-pohon-5" class="list-desc" style="display: none;">
                  <th class="list-desc header">Mahoni (Swietenia macrophilla)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-5" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    Tajuknya dapat membentuk kanopi payung yang sangat rimbun, batang slindris, percabangan banyak, kayunya keras, mengurangi polusi signifikan (filter udara yang baik).

                  </td>
                </tr>
                <tr id="nama-pohon-6" class="list-desc" style="display: none;">
                  <th class="list-desc header">Jati (Tectona grandis)</th>
                  <th class="list-desc"></th>
                </tr>
                <tr id="deskripsi-pohon-6" class="list-desc" style="display: none;">
                  <td class="list-desc">
                    Rata-rata ketinggian 9-11 meter, fungsi untuk furnitur berkualitas, dapat diasosiasikan dengan tanaman pangan di hutan rakyat, polikultur dengan tanaman kehutanan, atau monokultur untuk industri, kulit batang coklat kuning keabu-abuan, sebagian besar batang silindris dan lurus.

                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-sm-12 tree-group-bottom">
            <div class="table-responsive tree col-sm-3">
              <table class="table">
                <tr>
                  <th class="list-item header">Jarak Tanam (m)</th>
                </tr>
                <tr>
                  <td class="list-item">
                    <div class="form-group">
                      <select class="form-control" id="jarak_list">
                        <option value="9">3x3</option>
                        <option value="42">6x7</option>
                        <option value="24">8x3</option>
                        <option value="16">4x4</option>
                      </select>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div class="table-responsive tree" id="keterangan">
              <table>
                <tr>
                  <th>
                    Keterangan:
                  </th>
                  <th>
                    
                  </th>
                </tr>
                <tr>
                  <td>
                    3x3
                  </td>
                  <td>
                    Cocok untuk hutan monokultur dengan tujuan produksi industri
                  </td>
                </tr>
                <tr>
                  <td>
                    6x7
                  </td>
                  <td>
                    Cocok untuk kondisi topografi yang agak curam dan berundak
                  </td>
                </tr>
                <tr>
                  <td>
                    8x3
                  </td>
                  <td>
                    Cocok untuk hutan rakyat
                  </td>
                </tr>
                <tr>
                  <td>
                    4x4
                  </td>
                  <td>
                    Hutan tanaman industri
                  </td>
                </tr>
              </table>
            </div> 
          </div>
        </div>
      </form>  
    </div>
  </div>
  <div class="row">
    <input type="hidden" name="total-emisi-hidden" id="total-emisi-hidden" value="" >
    <input type="hidden" name="total-riap-1-hidden" id="total-riap-1-hidden" value=36.5>
    <input type="hidden" name="total-riap-2-hidden" id="total-riap-2-hidden" value=52.7>
    <input type="hidden" name="total-riap-3-hidden" id="total-riap-3-hidden" value=35.9>
    <input type="hidden" name="total-riap-4-hidden" id="total-riap-4-hidden" value=30.5>
    <input type="hidden" name="total-riap-5-hidden" id="total-riap-5-hidden" value=22.7>
    <input type="hidden" name="total-riap-6-hidden" id="total-riap-6-hidden" value=19.9>
    <input class="btn btn-success hitung-btn" id="hitung-btn" type="submit" value="Hitung!">
  </div>
</div>


<!-- Container (Pricing Section) -->
<div id="result" class="container-fluid">
  <div class="text-center">
    <h2>Result</h2>
    <p>Here are the result... :D</p>
  </div>

  <div class="result row">
    <div class="table-responsive table-result">
      <table class="table table ">
        <tr>
          <th class="list-edge-left header">Pohon</th>
          <th class="list-header header">Jumlah Pohon</th>
          <th class="list-header header">Luas Lahan (m<sup>2</sup>)</th>
          <th class="list-edge-right header">Total Biaya</th>
        </tr>
        <tr>
          <td class="list-result selected-pohon" id="selected-pohon"> </td>
          <td class="list-result jumlah-pohon" id="jumlah-pohon"> </td>
          <td class="list-result luas-lahan" id="luas-lahan"> </td>
          <td class="list-result total-cost-result">
            <span class="col-sm-6 cost-result">Rp.</span>
            <span class="col-sm-6 cost-result text-right" id="cost-result"> </span>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>

<div id="calculation"  class="container-fluid" style="visibility: hidden;">
  <div class="row">
    <p class="header calculation-header">Penjelasan Perhitungan</p>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  </div>
</div>

<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">CONTACT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Contact us and we'll get back to you.</p>
      <p><span class="glyphicon glyphicon-map-marker"></span> Chicago, US</p>
      <p><span class="glyphicon glyphicon-phone"></span> +00 1515151515</p>
      <p><span class="glyphicon glyphicon-envelope"></span> myemail@something.com</p>
    </div>
    <div class="col-sm-7 slideanim">
      <div class="row">
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
      </div>
      <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit">Send</button>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <a href="#myPage" title="To Top">
    <span class="glyphicon glyphicon-chevron-up"></span>
  </a>
  <p>© 2018 by CForest Team</a></p>
</footer>

<script src="{{ asset('js/home.js') }}"></script>

</body>
</html>

